#ifndef AUDIOENGINE_H
#define AUDIOENGINE_H

#include <SFML/Audio.hpp>
#include <boost/unordered_map.hpp>
#include <string>

namespace jon
{
    class AudioEngine
    {
        public:
            AudioEngine() { }
            AudioEngine(const char *file) { parseFile(file); }
            ~AudioEngine() { }

            void parseFile(const char*);
            void playSound(const char*);
        private:
            boost::unordered_map<std::string, sf::SoundBuffer> sounds;
    };
}

#endif // AUDIOENGINE_H
