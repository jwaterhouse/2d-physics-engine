#ifndef BEAM_H
#define BEAM_H

#include <SFML/Graphics.hpp>
#include "../include/IRigidBody.h"
#include "../include/Ray.h"

namespace jon
{
    class Beam
    {
        public:
            Beam(unsigned int, float);
            virtual ~Beam();

            const Ray* getPilotRay() const { return pilotRay; }
            void setStartPoint(sf::Vector2f val)
            {
                pilotRay->setStartPoint(val);
                resetBeam();
            }
            void setEndPoint(sf::Vector2f val)
            {
                pilotRay->setEndPoint(val);
                resetBeam();
            }
            unsigned int getNumRays() { return numRays; }
            float getSpacing() { return spacing; }
            void setSpacing(float val) { spacing = val; }
            Ray* getRays() { return rays; }

            void setColor(sf::Color);
            void resetBeam();

        protected:
        private:
            Ray* pilotRay;
            Ray* rays;
            unsigned int numRays;
            float spacing;
    };
}

#endif // BEAM_H
