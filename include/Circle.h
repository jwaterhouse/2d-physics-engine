#ifndef CIRCLE_H
#define CIRCLE_H

#include <SFML/Graphics.hpp>
#include "../include/IRigidBody.h"

namespace jon
{
    class Circle : public sf::CircleShape, public IRigidBody
    {
        public:
            Circle() : sf::CircleShape() { this->init(); }
            Circle(float f) : sf::CircleShape(f) { this->init(); }
            ~Circle() { }

            // Abstract classes
            virtual void init();
            virtual void updatePhysics(sf::Time);
            virtual void checkBounds();
            virtual void handleCollision(IRigidBody*);
            virtual bool pointWithin(sf::Vector2f*);
            virtual const sf::Vector2f getCentroid();
            virtual float getMomentOfInertia();
            virtual float getArea();
            virtual float getBoundingRadius();
        protected:
        private:
    };
}
#endif // CIRCLE_H
