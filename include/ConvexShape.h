#ifndef CONVEXSHAPE_H
#define CONVEXSHAPE_H

#include <SFML/Graphics.hpp>
#include "../include/IRigidBody.h"
#include "../include/inertiatensor2d.h"

namespace jon
{
    class ConvexShape : public sf::ConvexShape, public IRigidBody
    {
        public:
            // Default constructor
            ConvexShape() : sf::ConvexShape() { this->init(); }

            // Constructor
            // pointCount - how many points to initialise this shape with
            ConvexShape(int pointCount) : sf::ConvexShape(pointCount) { this->init(); }

            // Destructor
            ~ConvexShape() { }

            // Initialise the shape
            virtual void init();
            virtual void updatePhysics(sf::Time);
            virtual void checkBounds();
            virtual void handleCollision(IRigidBody*);
            virtual bool pointWithin(sf::Vector2f*);
            virtual const sf::Vector2f getCentroid();
            virtual float getMomentOfInertia();
            virtual float getArea();
            virtual float getBoundingRadius();
            virtual bool separatingAxisTheorem(ConvexShape*);
            virtual sf::Vector2f project(sf::Vector2f*);
        protected:
        private:
            inertia_tensor2d inertiaTensor2D;
            float radius;
    };
}

#endif // CONVEXSHAPE_H
