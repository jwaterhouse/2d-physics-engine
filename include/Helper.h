#ifndef HELPER_H_INCLUDED
#define HELPER_H_INCLUDED

#include <cmath>
#include <SFML/Graphics.hpp>
#include "../include/IRigidBody.h"

namespace jon
{
    static void handleCollision(jon::IRigidBody* shape, std::vector<jon::IRigidBody*>* shapes)
    {
        for (unsigned int i = 0; i < shapes->size(); ++i)
        {
            shape->handleCollision(shapes->at(i));
        }
    }

    static float dotProduct(sf::Vector2f* v1, sf::Vector2f* v2)
    {
        return v1->x * v2->x + v1->y * v2->y;
    }

    static float magnitude(sf::Vector2f* v)
    {
        return std::sqrt(v->x * v->x + v->y * v->y);
    }

    static float magnitudeSquared(sf::Vector2f* v)
    {
        return v->x * v->x + v->y * v->y;
    }

    static sf::Vector2f normalize(sf::Vector2f* v)
    {
        float m = jon::magnitude(v);
        return sf::Vector2f(v->x / m, v->y / m);
    }

    static sf::Vector2f perpendicular(sf::Vector2f* v)
    {
        return sf::Vector2f(-(v->y), v->x);
    }

    static bool contains(sf::Vector2f* range, float num)
    {
        float a = (range->x < range->y ? range->x : range->y);
        float b = (range->x < range->y ? range->y : range->x);
        return (num >= a && num <= b);
    }

    static bool overlap(sf::Vector2f* a, sf::Vector2f* b)
    {
        if (jon::contains(b, a->x)) return true;
        if (jon::contains(b, a->y)) return true;
        if (jon::contains(a, b->x)) return true;
        if (jon::contains(a, b->y)) return true;
        return false;
    }

    static float getAngle(float x, float y)
    {
        if (x >= 0 && y >= 0)
            return std::atan(y/x);
        else if (x <= 0 && y >= 0)
            return M_PI + std::atan(y/x);
        else if (x <= 0 && y <= 0)
            return M_PI + std::atan(y/x);
        else    // x >= 0 && y <= 0
            return 2*M_PI + std::atan(y/x);
    }
}

#endif // HELPER_H_INCLUDED
