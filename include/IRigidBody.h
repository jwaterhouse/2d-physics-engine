#ifndef IRIGIDBODY_H_INCLUDED
#define IRIGIDBODY_H_INCLUDED


#include <limits>
#include <SFML/Graphics.hpp>

namespace jon
{
    struct Force
    {
        float x;
        float y;
        float t;
    };

    // Define some common functions and variables for shapes manipulated by physics
    class IRigidBody
    {
        public:
            ~IRigidBody()
            {
                forceRegister.clear();
            }

            virtual void init()
            {
                this->setDensity(1.0f);
                this->setVelocity(sf::Vector2f(0.f, 0.f));
                this->setAngularVelocity(0.f);
                this->momentOfInertia = 0.f;
                this->setDrag(0.f);
                this->setBounds(sf::IntRect(0, 0, 0, 0));
                forceRegister = std::vector<Force*>();
            }

            virtual sf::Vector2f getVelocity() { return this->velocity; }
            virtual void setVelocity(sf::Vector2f velocity) { this->velocity = velocity; }
            virtual float getAngularVelocity() { return this->angularVelocity; }
            virtual void setAngularVelocity(float angularVelocity) { this->angularVelocity = angularVelocity; }
            virtual float getDensity()  { return this->density; }
            virtual void setDensity(float density) { this->density = density; }
            virtual float getMass() { return (this->getDensity() == std::numeric_limits<float>::max()) ? std::numeric_limits<float>::max() : this->getDensity() * this->getArea(); }
            virtual float getDrag() { return this->drag; }
            virtual void setDrag(float drag) { this->drag = drag; }
            virtual sf::IntRect getBounds() { return this->bounds; }
            virtual void setBounds(sf::IntRect bounds) { this->bounds = bounds; }
            virtual bool isMoveable() { return (this->getDensity() != std::numeric_limits<float>::max()); }

            virtual void addForce(sf::Vector2f force, sf::Time t)
            {
                Force* f = new Force();
                f->x = force.x;
                f->y = force.y;
                f->t = t.asSeconds();
                forceRegister.push_back(f);
            }

            virtual void applyForce(sf::Vector2f force, sf::Time t)
            {
                // Calculate the acceleration
                float ax = force.x / this->getMass();
                float ay = force.y / this->getMass();

                // Update the velocity
                float x = getVelocity().x + ax * t.asSeconds();
                float y = getVelocity().y + ay * t.asSeconds();

                // Apply drag
                x *= (1 - this->getDrag() * t.asSeconds());
                y *= (1 - this->getDrag() * t.asSeconds());

                setVelocity(sf::Vector2f(x, y));
            }

            virtual void applyForces(std::vector<sf::Vector2f> forces, sf::Time t)
            {
                sf::Vector2f totalForce(0.f, 0.f);
                for (unsigned int i = 0; i < forces.size(); ++i)
                {
                    totalForce += forces[i];
                }

                // Apply the totalled force
                this->applyForce(totalForce, t);
            }

            virtual void applyForces(sf::Time t)
            {
                if (forceRegister.size() == 0) return; // No forces to apply
                if (t.asSeconds() <= 0) return; // No time to apply force;

                float minTime = forceRegister.at(0)->t;
                for (unsigned int i = 1; i < forceRegister.size(); ++i)
                {
                    if (forceRegister.at(i)->t < minTime)
                    {
                        minTime =  forceRegister.at(i)->t;
                    }
                }

                minTime = std::min(minTime, t.asSeconds());
                std::vector<sf::Vector2f> forces(0);
                std::vector<Force*>::iterator it = forceRegister.begin();
                while (it != forceRegister.end())
                {
                    forces.push_back(sf::Vector2f((*it)->x, (*it)->y));
                    (*it)->t -= minTime;
                    if ((*it)->t <= 0)
                    {
                        delete (*it);
                        forceRegister.erase(it);
                    }
                }

                applyForces(forces, sf::seconds(minTime));
                applyForces(t - sf::seconds(minTime));
            }

            // Pure virtual functions
            virtual void updatePhysics(sf::Time) = 0;
            virtual void checkBounds() = 0;
            virtual void handleCollision(IRigidBody*) = 0;
            virtual bool pointWithin(sf::Vector2f*h) = 0;
            virtual const sf::Vector2f getCentroid() = 0;
            virtual float getMomentOfInertia() = 0;
            virtual float getArea() = 0;
            virtual float getBoundingRadius() = 0;

        protected:
            float momentOfInertia;

        private:
            sf::Vector2f velocity;
            float angularVelocity;
            float density;
            float drag;
            sf::IntRect bounds;
            std::vector<Force*> forceRegister;
    };
}
#endif // IRIGIDBODY_H_INCLUDED
