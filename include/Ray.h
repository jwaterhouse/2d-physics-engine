#ifndef RAY_H
#define RAY_H

#include <SFML/Graphics.hpp>
#include "../include/IRigidBody.h"

namespace jon
{
    struct RayCollision
    {
        bool collision;
        sf::Vector2f enterPoint;
        sf::Vector2f exitPoint;
    };

    class Ray
    {
        public:
            Ray();
            Ray(sf::Color);
            virtual ~Ray();
            void init();
            sf::Vertex getStartPoint() { return points[0].position; }
            void setStartPoint(sf::Vector2f val) { points[0].position = val; }
            sf::Vertex getEndPoint() { return points[1].position; }
            void setEndPoint(sf::Vector2f val) { points[1].position = val; }
            const sf::Vertex* getVertexArray() const { return points; }
            void setColor (sf::Color color) {
                points[0].color = color;
                points[1].color = color;
            }

            RayCollision checkIntersection(IRigidBody*);

        protected:
        private:
            sf::Vertex *points;
    };
}

#endif // RAY_H
