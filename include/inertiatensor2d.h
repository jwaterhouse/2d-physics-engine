#ifndef INERTIA_TENSOR2D_H
#define INERTIA_TENSOR2D_H

/*
Adapted from the code by Michael Kallay
http://jgt.akpeters.com/papers/Kallay06/

Christopher Batty, November 26, 2007
*/

class inertia_tensor2d
{
private:
  // Data members
  double _m;                              // Mass
  double _Cx, _Cy;                   // Centroid
  double _xx,_yy;    // Moment of inertia tensor

public:
   inertia_tensor2d() {
      _m = 0;
      _Cx = 0;
      _Cy = 0;
      _xx = 0;
      _yy = 0;
   }

/**********************************************************************
Add the contribution of a segment to the mass properties.
Call this method for each one of the mesh's edges.
The order of vertices must be counter-clockwise around the exterior.
**********************************************************************/
  void add_edge_contribution(
			       double x1, double y1,    // Segments's vertex 1
			       double x2, double y2,    // Segment's vertex 2
			       float density);          // The density of the object


/**********************************************************************
This method is called to obtain the results.
This call modifies the internal data; calling it again will return
incorrect results.
**********************************************************************/
  void get_results(
		  double & m,                   // Total mass
		  double & Cx,  double & Cy,   // Centroid
		  double & Izz);                // Moment of inertia

};


#endif
