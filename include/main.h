#ifndef MAIN_H
#define MAIN_H

#include "../include/IRigidBody.h"

class main
{
    public:
        main();
        virtual ~main();
        static float getAngle(float, float);
        static void handleCollision(jon::IRigidBody*, std::vector<jon::IRigidBody*>*);
        static float dotProduct(sf::Vector2f*, sf::Vector2f*);
        static float magnitude(sf::Vector2f*);
        static float magnitudeSquared(sf::Vector2f*);
    protected:
    private:
};

#endif // MAIN_H
