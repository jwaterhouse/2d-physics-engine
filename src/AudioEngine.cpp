#include "../include/AudioEngine.h"
#include <boost/tokenizer.hpp>
#include <fstream>
#include <sstream>
#include <string>

using namespace jon;

void AudioEngine::parseFile(const char* file)
{
    std::ifstream infile(file);

    std::string line;
    while (std::getline(infile, line))
    {
        boost::char_separator<char> sep(" ", "", boost::drop_empty_tokens);
        boost::tokenizer<boost::char_separator<char> > tok(line, sep);

        boost::tokenizer<boost::char_separator<char> >::iterator tok_iter = tok.begin();
        std::string name(*tok_iter);
        ++tok_iter;
        std::string soundFile(*tok_iter);

        //std::cout << name << ", " << soundFile << "\n";
        sf::SoundBuffer buffer;
        if (buffer.loadFromFile(soundFile.c_str()))
            sounds[name] = buffer;
    }
}

void AudioEngine::playSound(const char* name)
{
    // find the sound
    if(sounds.find(name) != sounds.end())
    {
        sf::SoundBuffer soundBuf = sounds.at(name);
        sf::Sound sound(soundBuf);
        sound.setLoop(true);
        sound.play();
    }
}
