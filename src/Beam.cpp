#include "../include/Beam.h"
#include "../include/Helper.h"

using namespace jon;

Beam::Beam(unsigned int numRays, float spacing)
{
    this->numRays = numRays;
    this->spacing = spacing;
    pilotRay = new Ray();
    rays = new Ray[numRays];
}

Beam::~Beam()
{
    if (pilotRay)
    {
        delete pilotRay;
        pilotRay = NULL;
    }
    if (rays)
    {
        delete [] rays;
        rays = NULL;
    }
}

void Beam::setColor(sf::Color color)
{
    for (unsigned int i = 0; i < numRays; ++i)
    {
        rays[i].setColor(color);
    }
}

void Beam::resetBeam()
{
        // Trye and draw another ray beside this one
        /*
        float dist = 5.f;
        sf::Vector2f rayVector = ray->getEndPoint().position - ray->getStartPoint().position;
        sf::Vector2f n(-rayVector.y, rayVector.x);
        jon::Ray normalRay(sf::Color::Red);
        normalRay.setStartPoint(ray->getStartPoint().position);
        normalRay.setEndPoint(n + ray->getStartPoint().position);
        window.draw(normalRay.getVertexArray(), 2, sf::Lines);

        float an = jon::magnitude(&n);
        sf::Vector2f nextRayOrigin = n * (dist / an);
        jon::Ray nextRay(sf::Color::Green);
        nextRay.setStartPoint(nextRayOrigin + ray->getStartPoint().position);
        nextRay.setEndPoint(nextRayOrigin + ray->getEndPoint().position);
        window.draw(nextRay.getVertexArray(), 2, sf::Lines);
        */
        if (pilotRay->getStartPoint().position == pilotRay->getEndPoint().position) return;

        sf::Vector2f rayVector = pilotRay->getEndPoint().position - pilotRay->getStartPoint().position;
        sf::Vector2f n(-rayVector.y, rayVector.x);
        float an = jon::magnitude(&n);

        float dist = -spacing * (numRays - 1) / 2;
        for (unsigned int i = 0; i < numRays; ++i)
        {
            float iDist = dist + spacing * (float)i;
            sf::Vector2f iVector = n * iDist / an;
            rays[i].setStartPoint(pilotRay->getStartPoint().position + iVector);
            rays[i].setEndPoint(pilotRay->getEndPoint().position + iVector);
        }
}
