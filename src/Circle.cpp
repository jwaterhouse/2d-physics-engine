#include "../include/Circle.h"
#include "../include/Helper.h"
#include <cmath>
#include <iostream>

using namespace jon;

void Circle::init()
{
    IRigidBody::init();
}

// Update the physics for the circle over time
void Circle::updatePhysics(sf::Time t)
{
    // Calculate and apply the displacement
    this->move(getVelocity().x * t.asSeconds(), getVelocity().y * t.asSeconds());
    this->checkBounds();
}

// If the circle position is outside its bounds, it needs to bounce back
void Circle::checkBounds()
{
    float x = getPosition().x;
    float y = getPosition().y;
    float vx = getVelocity().x;
    float vy = getVelocity().y;

    int leftLimit = getBounds().left;
    int rightLimit = getBounds().left + getBounds().width;
    int upperLimit = getBounds().top;
    int lowerLimit = getBounds().top + getBounds().height;

    if (x > (rightLimit - getRadius() * 2))
    {
        x = (rightLimit - getRadius() * 2) * 2 - x;
        vx = -vx;
    }
    else if (x < leftLimit)
    {
        x = leftLimit * 2 - x;
        vx = -vx;
    }

    if (y > (lowerLimit - getRadius() * 2))
    {
        y = (lowerLimit - getRadius() * 2) * 2 - y;
        vy = -vy;
    }
    else if (y < upperLimit)
    {
        y = upperLimit * 2 - y;
        vy = -vy;
    }

    setVelocity(sf::Vector2f(vx, vy));
    setPosition(x, y);
}

void Circle::handleCollision(IRigidBody* shape)
{
    if (this == shape) return;  // Don't handle collision with self
    if (this->isMoveable() == false && shape->isMoveable() == false) return;    // Immoveable objects shouldn't collide with each other

    Circle* circle = dynamic_cast<Circle*>(shape);

    if (circle != 0)    // we have a circle
    {
        sf::Vector2f centre1 = this->getTransform().transformPoint(this->getCentroid());
        sf::Vector2f centre2 = circle->getTransform().transformPoint(circle->getCentroid());
        sf::Vector2f diff = centre1 - centre2;

        float lengthSquared = diff.x * diff.x + diff.y * diff.y;

        if (lengthSquared < std::pow(this->getRadius() + circle->getRadius(), 2))
        {
            // Masses
            float m1 = this->getMass();
            float m2 = circle->getMass();

            // Normal
            sf::Vector2f n = diff;
            float an = std::sqrt(n.x * n.x + n.y * n.y);
            n.x = n.x / an;
            n.y = n.y / an;

            // Length of the component of each of the movement vectors along n
            sf::Vector2f u1 = this->getVelocity();
            sf::Vector2f u2 = circle->getVelocity();
            float a1 = jon::dotProduct(&u1, &n);
            float a2 = jon::dotProduct(&u2, &n);

            float optimisedP  = (2.f * (a1 - a2)) / (m1 + m2);

            // Calculate new velocities
            sf::Vector2f v1;
            sf::Vector2f v2;

            // Set the velocities for each circle
            if (this->isMoveable() == false)
            {
                v1 = this->getVelocity();
                v2 = circle->getVelocity() + 2.f * optimisedP * m1 * n;
            }
            else if (circle->isMoveable() == false)
            {
                v1 = this->getVelocity() - 2.f * optimisedP * m2 * n;
                v2 = circle->getVelocity();
            }
            else
            {
                v1 = this->getVelocity() - optimisedP * m2 * n;
                v2 = circle->getVelocity() + optimisedP * m1 * n;
            }

            this->setVelocity(sf::Vector2f(v1.x, v1.y));
            circle->setVelocity(sf::Vector2f(v2.x, v2.y));

            // Calculate the distance travelled by each circle
            float length = std::sqrt(diff.x * diff.x + diff.y * diff.y);
            float overlap = this->getRadius() + circle->getRadius() - length;
            sf::Vector2f ov = n * overlap;

            this->move(ov.x / 2.f, ov.y / 2.f);
            circle->move(-ov.x / 2.f, -ov.y / 2.f);

            this->checkBounds();
            circle->checkBounds();
        }
    }
}

bool Circle::pointWithin(sf::Vector2f* point)
{
    sf::Vector2f centre(this->getPosition().x + this->getRadius(), this->getPosition().y + this->getRadius());
    sf::Vector2f diff = centre - *point;
    return diff.x * diff.x + diff.y * diff.y < (this->getRadius() * this->getRadius());
}

const sf::Vector2f Circle::getCentroid()
{
    return sf::Vector2f(this->getRadius(), this->getRadius());
}

float Circle::getBoundingRadius()
{
    return this->getRadius();
}

float Circle::getMomentOfInertia()
{
    if (this->momentOfInertia != 0.f) return this->momentOfInertia;

    this->momentOfInertia = this->getMass() * std::pow(this->getRadius(), 2) / 2.f;

    return this->momentOfInertia;
}

float Circle::getArea()
{
    return M_PI * std::pow(this->getRadius(), 2);
}
