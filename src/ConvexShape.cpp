#include "../include/ConvexShape.h"
#include "../include/CircleShape.h"
#include "../include/Helper.h"
#include <cmath>
#include <iostream>
#include <limits>

using namespace jon;

void ConvexShape::init()
{
    IRigidBody::init();
    this->radius = 0.f;
    this->inertiaTensor2D = inertia_tensor2d();
}

// Calculate whether the given point is within the bounds of the polygon
bool ConvexShape::pointWithin(sf::Vector2f* point)
{
    unsigned int i = 0;
    unsigned int j = this->getPointCount() - 1;
    bool oddNodes = false;
    float x = point->x;
    float y = point->y;

    for(i = 0; i < this->getPointCount(); ++i)
    {
        sf::Vector2f p1= this->getPoint(i);
        sf::Vector2f p2 = this->getPoint(j);
        if (((p1.y < y && p2.y >= y) || (p2.y < y && p1.y >= y)) && (p1.x <= x || p2.x <= x))
        {
            oddNodes ^= (p1.x + (y - p1.y) / (p2.x - p1.y) * (p2.x - p1.x) < x);
        }
        j=i;
    }

    return oddNodes;
}

// Update the physics for the CircleShape over time
void ConvexShape::updatePhysics(sf::Time t)
{
    // Calculate and apply the displacement
    this->move(getVelocity().x * t.asSeconds(), getVelocity().y * t.asSeconds());
    this->setOrigin(this->getCentroid().x, this->getCentroid().y);
    this->rotate(this->getAngularVelocity() * t.asSeconds() * 360.f / (2 * M_PI));
    this->checkBounds();
}

// If the shape is outside its bounds, it needs to bounce back
void ConvexShape::checkBounds()
{
    int leftLimit = this->getBounds().left;
    int rightLimit = this->getBounds().left + getBounds().width;
    int upperLimit = this->getBounds().top;
    int lowerLimit = this->getBounds().top + getBounds().height;

    for (unsigned int i = 0; i < this->getPointCount(); ++i)
    {
        bool collision = false;
        float x = 0.f;
        float y = 0.f;
        sf::Vector2f n(0.f, 0.f);

        sf::Vector2f p = this->getTransform().transformPoint(this->getPoint(i));
        if (p.x > rightLimit)
        {
            x = rightLimit - p.x;
            n.x = -1.f;
            collision = true;
        }
        else if (p.x < leftLimit)
        {
            x = leftLimit - p.x;
            n.x = 1.f;
            collision = true;
        }

        if (p.y > lowerLimit)
        {
            y = lowerLimit - p.y;
            n.y = -1.f;
            collision = true;
        }
        else if (p.y < upperLimit)
        {
            y = upperLimit - p.y;
            n.y = 1.f;
            collision = true;
        }

        if (collision)
        {
            this->move(x, y);

            // Make normal a unit vector.
            float an = std::sqrt(n.x * n.x + n.y * n.y);
            n.x = n.x / an;
            n.y = n.y / an;

            float e = 1.f;
            sf::Vector2f rap = p - this->getTransform().transformPoint(this->getCentroid());
            sf::Vector2f rapPerp(-rap.y, rap.x);
            sf::Vector2f vap = this->getVelocity() + this->getAngularVelocity() * rapPerp;

            float impulse = -(1.f + e) * jon::dotProduct(&vap, &n) /
                            (1.f / this->getMass() +
                            std::pow(jon::dotProduct(&rapPerp, &n), 2.f) / this->getMomentOfInertia());

            sf::Vector2f va = this->getVelocity() + (impulse / this->getMass()) * n;
            float wa = this->getAngularVelocity() + impulse * jon::dotProduct(&rapPerp, &n) / this->getMomentOfInertia();
            this->setVelocity(va);
            this->setAngularVelocity(wa);
        }
    }
}

void ConvexShape::handleCollision(IRigidBody* shape)
{
    if (this == shape) return;  // Don't handle collision with self
    if (this->isMoveable() == false && shape->isMoveable() == false) return;    // Immoveable objects shouldn't collide with each other

    CircleShape* circle = dynamic_cast<CircleShape*>(shape);
    if (circle != 0)
    {
        // Get the convex shape's transform
        sf::Transform t = this->getTransform();

        // Check if the CircleShape and the bounding radius of the polygon overlap
        sf::Vector2f centre1 = t.transformPoint(this->getCentroid());
        sf::Vector2f centre2 = circle->getTransform().transformPoint(circle->getCentroid());
        sf::Vector2f diff = centre1 - centre2;

        float lengthSquared = diff.x * diff.x + diff.y * diff.y;

        if (lengthSquared >= std::pow(this->getBoundingRadius() + circle->getRadius(), 2)) return;

        if (this->getPointCount() == 1)
        {
            // Special case for collision with a single point?
        }

        // the centre point of the circle
        sf::Vector2f centre = circle->getTransform().transformPoint(circle->getCentroid());

        // nearest point on an edge of the polygon to the centre of the circle
        sf::Vector2f closestPoint = t.transformPoint(this->getPoint(0));

        // nearest vertex of the polygon to the centre of the circle
        sf::Vector2f closestVertex = t.transformPoint(this->getPoint(0));

        unsigned int i = 0;
        unsigned int j = this->getPointCount() - 1;

        for (i = 0; i < this->getPointCount(); ++i)
        {
            // transform the points
            sf::Vector2f p1 = t.transformPoint(this->getPoint(i));
            sf::Vector2f p2 = t.transformPoint(this->getPoint(j));

            sf::Vector2f nextPoint(0.f, 0.f);

            // Check for closest vertex
            float oldDistSquared = std::pow(centre.x - closestVertex.x, 2) + std::pow(centre.y - closestVertex.y, 2);
            float newDistSquared = std::pow(centre.x - p1.x, 2) + std::pow(centre.y - p1.y, 2);
            if (newDistSquared < oldDistSquared) closestVertex = p1;

            float edgeLengthSquared = std::pow(p1.x - p2.x, 2) + std::pow(p1.y - p2.y, 2);
            if (edgeLengthSquared == 0) // Edge is actually a point
            {
                nextPoint = p1;
            }
            else
            {
                float t = ((centre.x - p1.x) * (p2.x - p1.x) + (centre.y - p1.y) * (p2.y - p1.y)) / edgeLengthSquared;
                if (t < 0.f)
                {
                    nextPoint = p1;
                }
                else if (t > 1.f)
                {
                    nextPoint = p2;
                }
                else
                {
                    nextPoint = sf::Vector2f(p1.x + t * (p2.x - p1.x), p1.y + t * (p2.y - p1.y));
                }
            }
            sf::Vector2f closestVector = centre - closestPoint;
            sf::Vector2f nextVector = centre - nextPoint;

            if (nextVector.x * nextVector.x + nextVector.y * nextVector.y < closestVector.x * closestVector.x + closestVector.y * closestVector.y)
            {
                closestPoint = nextPoint;
            }
            j = i;
        }

        // Now that we know the closest point on polygon, compute whether collision
        float closestPointDistSquared = std::pow(centre.x - closestPoint.x, 2) + std::pow(centre.y - closestPoint.y, 2);

        if (closestPointDistSquared < circle->getRadius() * circle->getRadius())
        {
            // Get normal projecting from shape B's edge
            sf::Vector2f n = closestPoint - centre;
            float an = std::sqrt(n.x * n.x + n.y * n.y);
            n.x = n.x / an;
            n.y = n.y / an;

            float e = 1.f;
            sf::Vector2f rap = closestPoint - this->getTransform().transformPoint(this->getCentroid());
            sf::Vector2f rbp = closestPoint - circle->getTransform().transformPoint(circle->getCentroid());
            sf::Vector2f rapPerp(-rap.y, rap.x);
            sf::Vector2f rbpPerp(-rbp.x, rbp.y);
            sf::Vector2f vap = this->getVelocity() + this->getAngularVelocity() * rapPerp;
            sf::Vector2f vbp = circle->getVelocity() + circle->getAngularVelocity() * rbpPerp;
            sf::Vector2f vab = vap - vbp;

            float impulse = -(1.f + e) * jon::dotProduct(&vab, &n) /
                            ((1.f / this->getMass() + 1.f / circle->getMass()) +
                            std::pow(jon::dotProduct(&rapPerp, &n), 2.f) / this->getMomentOfInertia() +
                            std::pow(jon::dotProduct(&rbpPerp, &n), 2.f) / circle->getMomentOfInertia());

            sf::Vector2f va = this->getVelocity() + (impulse / this->getMass()) * n;
            float wa = this->getAngularVelocity() + impulse * jon::dotProduct(&rapPerp, &n) / this->getMomentOfInertia();
            this->setVelocity(va);
            this->setAngularVelocity(wa);

            sf::Vector2f vb = circle->getVelocity() - (impulse / circle->getMass()) * n;
            float wb = circle->getAngularVelocity() - impulse * jon::dotProduct(&rbpPerp, &n) / circle->getMomentOfInertia();
            circle->setVelocity(vb);
            circle->setAngularVelocity(wb);

            // Calculate the overlap
            float length = std::sqrt(closestPointDistSquared);
            float overlap = circle->getRadius() - length;
            sf::Vector2f ov = n * overlap;

            // Separate the shapes
            this->move(ov.x, ov.y);
            circle->move(-ov.x, -ov.y);

            this->checkBounds();
            circle->checkBounds();
        }
    }

    ConvexShape* convexShape = dynamic_cast<ConvexShape*>(shape);
    if (convexShape != 0)
    {
        // Get the convex shape's transform
        sf::Transform t = this->getTransform();

        // Check if the CircleShape and the bounding radius of the polygon overlap
        sf::Vector2f centre1 = t.transformPoint(this->getCentroid());
        sf::Vector2f centre2 = convexShape->getTransform().transformPoint(convexShape->getCentroid());
        sf::Vector2f diff = centre1 - centre2;
        float lengthSquared = diff.x * diff.x + diff.y * diff.y;
        if (lengthSquared >= std::pow(this->getBoundingRadius() + convexShape->getBoundingRadius(), 2)) return;

        // Now check the separting axis theorem
        if (this->separatingAxisTheorem(convexShape)) return;

        // These pointers witll store points that are found within the other shape's area
        sf::Vector2f* thisPoint = 0;
        sf::Vector2f* convexShapePoint = 0;

        // Check if any of the convexShape's points lie within the area of this shape
        for (unsigned int i = 0; i < convexShape->getPointCount(); ++i)
        {
            // Apply the convexShape transform to the point, and then the inverse transform of this shape
            // so that it will be relative to this shape
            sf::Transform convexShapeTransform = convexShape->getTransform();
            sf::Transform thisInverseTransform = this->getInverseTransform();

            sf::Vector2f p = thisInverseTransform.transformPoint(convexShapeTransform.transformPoint(convexShape->getPoint(i)));

            if(this->pointWithin(&p) == true)
            {
                // We have a collision
                *convexShapePoint = convexShapeTransform.transformPoint(convexShape->getPoint(i));
            }
        }

        // Check if any of the convexShape's points lie within the area of this shape
        for (unsigned int i = 0; i < this->getPointCount(); ++i)
        {
            // Apply the convexShape transform to the point, and then the inverse transform of this shape
            // so that it will be relative to this shape
            sf::Transform thisTransform = this->getTransform();
            sf::Transform convexShapeInverseTransform = convexShape->getInverseTransform();

            sf::Vector2f p = convexShapeInverseTransform.transformPoint(thisTransform.transformPoint(this->getPoint(i)));

            if(convexShape->pointWithin(&p) == true)
            {
                // We have a collision
                *thisPoint = thisTransform.transformPoint(this->getPoint(i));
            }
        }



        sf::Vector2f collisionPoint;
        if (thisPoint != 0 && convexShapePoint == 0) collisionPoint = *thisPoint;
        else if (thisPoint == 0 && convexShapePoint != 0) collisionPoint = *convexShapePoint;
        else if (thisPoint != 0 && convexShapePoint != 0) collisionPoint = sf::Vector2f((thisPoint->x + convexShapePoint->x) / 2, (thisPoint->y + convexShapePoint->y) / 2);
        else /*if (thisPoint == 0 && convexShapePoint == 0)*/ return; // this should never happen...

    }
}

bool ConvexShape::separatingAxisTheorem(ConvexShape* shape)
{
//    bool sat(polygon a, polygon b){
//    for (int i = 0; i < a.edges.length; i++){
//        vector axis = a.edges[i].direction; // Get the direction vector of the edge
//        axis = vec_normal(axis); // We need to find the normal of the axis vector.
//        axis = vec_unit(axis); // We also need to "normalize" this vector, or make its length/magnitude equal to //1/

//        // Find the projection of the two polygons onto the axis
//        segment proj_a = project(a, axis), proj_b = project(b, axis);

 //       if(!seg_overlap(proj_a, proj_b)) return false; // If they do not overlap, then return false
 //   }
 //   ... // Same thing for polygon b
    // At this point, we know that there were always intersections, hence the two polygons must be colliding
 //   return true;

    for (unsigned int i = 0; i < this->getPointCount(); ++i)
    {
        unsigned int j = (i < this->getPointCount() - 1 ? i + 1 : 0);
        sf::Vector2f dir(this->getPoint(j).x - this->getPoint(i).x, this->getPoint(j).y - this->getPoint(i).y);
        sf::Vector2f n = jon::perpendicular(&dir);
        n = jon::normalize(&n);

        sf::Vector2f projA = this->project(&n);
        sf::Vector2f projB = shape->project(&n);

        if (!jon::overlap(&projA, &projB)) return false;
    }

    for (unsigned int i = 0; i < shape->getPointCount(); ++i)
    {
        unsigned int j = (i < shape->getPointCount() - 1 ? i + 1 : 0);
        sf::Vector2f dir(shape->getPoint(j).x - shape->getPoint(i).x, shape->getPoint(j).y - shape->getPoint(i).y);
        sf::Vector2f n = jon::perpendicular(&dir);
        n = jon::normalize(&n);

        sf::Vector2f projA = this->project(&n);
        sf::Vector2f projB = shape->project(&n);

        if (!jon::overlap(&projA, &projB)) return false;
    }

    return true;
}

sf::Vector2f ConvexShape::project(sf::Vector2f* axis)
{
	*axis = jon::normalize(axis);

    // min and max are the start and finish points
    sf::Vector2f p = this->getPoint(0);
	float min = jon::dotProduct(&p, axis);
	float max = min;

	for (unsigned int i = 0; i < this->getPointCount(); i++)
	{
	    p = this->getPoint(i);
		float proj = jon::dotProduct(&p, axis); // find the projection of every point on the polygon onto the line.
		if (proj < min) min = proj;
		if (proj > max) max = proj;
	}
	return sf::Vector2f(min, max);
}

// http://en.wikipedia.org/wiki/Centroid
const sf::Vector2f ConvexShape::getCentroid()
{
    float a = 0.f;
    float cx = 0.f;
    float cy = 0;

    for (unsigned int i = 0; i < this->getPointCount(); ++i)
    {
        unsigned int j = ((i + 1) < this->getPointCount() ? i + 1 : 0);
        sf::Vector2f p1 = this->getPoint(i);
        sf::Vector2f p2 = this->getPoint(j);

        cx += (p1.x + p2.x) * (p1.x * p2.y - p2.x * p1.y);
        cy += (p1.y + p2.y) * (p1.x * p2.y - p2.x * p1.y);
        a += (p1.x * p2.y - p2.x * p1.y);
    }

    a /= 2.f;
    cx /= (6.f * a);
    cy /= (6.f * a);

    return sf::Vector2f(cx, cy);
}

// Gets the radius of the smallest circle that contains the polygon
float ConvexShape::getBoundingRadius()
{
    if (this->radius != 0.f) return this->radius;

    float maxLengthSquared = 0.f;
    sf::Vector2f centre = this->getCentroid();
    for (unsigned int i = 0; i < this->getPointCount() - 1; ++i)
    {
        sf::Vector2f p = this->getPoint(i) + this->getPosition();
        sf::Vector2f diff = p - centre;
        float lengthSquared = diff.x * diff.x + diff.y * diff.y;
        if (lengthSquared > maxLengthSquared) maxLengthSquared = lengthSquared;
    }
    this->radius = std::sqrt(maxLengthSquared);
    return this->radius;
}

float ConvexShape::getMomentOfInertia()
{
    if (this->momentOfInertia != 0.f) return momentOfInertia;

    for (unsigned int i = 0; i < this->getPointCount(); ++i)
    {
        unsigned int j = i + 1;
        if (j >= this->getPointCount()) j = 0;

        sf::Vector2f pi = this->getPoint(i);
        sf::Vector2f pj = this->getPoint(j);

        this->inertiaTensor2D.add_edge_contribution(pi.x, pi.y, pj.x, pj.y, this->getDensity());
    }

    double m = 0.0;
    double cx = 0.0;
    double cy = 0.0;
    double izz = 0.0;

    this->inertiaTensor2D.get_results(m, cx, cy, izz);

    this->momentOfInertia = (float)izz;
    return this->momentOfInertia;
}

float ConvexShape::getArea()
{
    float a = 0.f;
    for (unsigned int i = 0; i < this->getPointCount() - 1; ++i)
    {
        unsigned int j = i + 1;

        sf::Vector2f pi = this->getPoint(i);
        sf::Vector2f pj = this->getPoint(j);

        a += pi.x * pj.y - pj.x * pi.y;
    }

    return a / 2;
}
