#include "../include/Ray.h"
#include "../include/ConvexShape.h"
#include "../include/CircleShape.h"
#include "../include/Helper.h"

#include <cmath>
#include <iostream>

using namespace jon;

Ray::Ray()
{
    init();
}

Ray::Ray(sf::Color color)
{
    init();
    points[0].color = color;
    points[1].color = color;
}

Ray::~Ray()
{
    if (points)
    {
        delete [] points;
        points = NULL;
    }
}

void Ray::init()
{
    points = new sf::Vertex[2];
    points[0] = sf::Vertex(sf::Vector2f(0, 0));
    points[1] = sf::Vertex(sf::Vector2f(0, 0));
}

RayCollision Ray::checkIntersection(IRigidBody* shape)
{
    RayCollision rc;
    rc.collision = false;
    if (shape->isMoveable() == false) return rc;    // Immoveable object, can't affect it

    CircleShape* circle = dynamic_cast<CircleShape*>(shape);

    if (circle != 0)
    {
        sf::Vector2f e = this->getStartPoint().position;
        sf::Vector2f l = this->getEndPoint().position;
        sf::Vector2f d = l - e;
        sf::Vector2f f = e - circle->getTransform().transformPoint(circle->getCentroid());

        float r = circle->getRadius();
        float a = jon::dotProduct(&d, &d);
        float b = 2.f * jon::dotProduct(&f, &d);
        float c = jon::dotProduct(&f, &f) - r * r ;

        float discriminant = b * b - 4.f * a * c;
        if( discriminant < 0.f )
        {
            // no intersection
        }
        else
        {
            // ray didn't totally miss sphere,
            // so there is a solution to
            // the equation.


            discriminant = std::sqrt(discriminant);
            // either solution may be on or off the ray so need to test both
            float t1 = (-b - discriminant) / (2.f * a);
            float t2 = (-b + discriminant) / (2.f * a);

            /*
            if( t1 >= 0 && t1 <= 1 )
            {
                // t1 solution on is ON THE RAY.
            }
            else
            {
                // t1 solution "out of range" of ray
            }

            if( t2 >= 0 && t2 <= 1 )
            {
                // t2 solution on is ON THE RAY.
            }
            else
            {
                // t2 solution "out of range" of ray
            }
            */

            sf::Vector2f intersect1 = e + t1 * d;
            sf::Vector2f intersect2 = e + t2 * d;

            //float angle = std::acos(jon::dotProduct(&d, &direction) / (jon::magnitude(&d) * jon::magnitude(&direction)));
            //std::cout << "t: " << t1 << " " << t2 <<  " Angle: " << angle << "\n";

            rc.enterPoint = intersect1;
            rc.exitPoint = intersect2;
            rc.collision = true;

            return rc;
        }
    }
    return rc;
}
