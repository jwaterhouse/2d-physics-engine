#include "../include/inertiatensor2d.h"

/*
Adapted from the code by Michael Kallay
http://jgt.akpeters.com/papers/Kallay06/

Christopher Batty, November 26, 2007
*/

void inertia_tensor2d::add_edge_contribution(
					      double x1, double y1,  // edge's vertex 1
					      double x2, double y2, // edge's vertex 2
					      float density)
{
  // Signed area of this triangle (where 3rd vertex is the origin).
  double v = x1*y2 - x2*y1;

  // Contribution to the mass
  _m += v * (double)density;

  // Contribution to the centroid
  double x4 = x1 + x2;           _Cx += (v * x4);
  double y4 = y1 + y2;           _Cy += (v * y4);

  // Contribution to moment of inertia monomials
  _xx += v * (x1*x1 + x2*x2 + x4*x4);
  _yy += v * (y1*y1 + y2*y2 + y4*y4);
}


void inertia_tensor2d::get_results(
				 double & m,                               // Total mass
				 double & Cx,  double & Cy,// Centroid
				 double & Izz) // Moment of inertia
{
  // Centroid.
  // The case _m = 0 needs to be addressed here.
  double r = 1.0 / (3 * _m);
  Cx = _Cx * r;
  Cy = _Cy * r;

  // Mass
  m = _m / 2;

  // Moment of inertia about the centroid.
  r = 1.0 / 24;

  _xx = _xx * r - m * Cx*Cx;
  _yy = _yy * r - m * Cy*Cy;

  Izz = _xx + _yy;
}
