#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>
#include <cmath>
#include <iostream>
#include <cstdlib>
#include <sstream>
#include <limits>
#include "../include/main.h"
#include "../include/AudioEngine.h"
#include "../include/Helper.h"
#include "../include/CircleShape.h"
#include "../include/ConvexShape.h"
#include "../include/Ray.h"
#include "../include/Beam.h"

int main()
{

    sf::ContextSettings settings;
    settings.antialiasingLevel = 8;
    sf::RenderWindow window(sf::VideoMode(768 / 2, 1280 / 2), "Physics Test!", sf::Style::Default, settings);

    // Declare a new font
    sf::Font font;
    // Load it from a file
    if (!font.loadFromFile("C:/Windows/Fonts/Arial.ttf"))
    {
        return 1;
    }

    // Create an audio engine
    jon::AudioEngine audio("./resources/sounds.info");
    audio.playSound("bump");

    /*sf::SoundBuffer soundBuf;
    if(!soundBuf.loadFromFile("./resources/sounds/bump.wav"))
        return -1;

    sf::Sound sound(soundBuf);
    //sound.setLoop(true);
    sound.play();*/


    sf::Text fps("000", font);
    fps.setColor(sf::Color::Yellow);
    fps.setPosition(5, 5);

    std::vector<sf::Shape*> shapes(0);
    std::vector<jon::IRigidBody*> physicsShapes(0);

    jon::Beam* beam = new jon::Beam(7, 10.f);
    beam->setStartPoint(sf::Vector2f(window.getSize().x / 2, window.getSize().y));
    beam->setEndPoint(sf::Vector2f(300, 200));
    beam->setColor(sf::Color::Green);

    jon::CircleShape* playerCircle = new jon::CircleShape(25.f);
    playerCircle->setFillColor(sf::Color::Blue);
    playerCircle->setPosition(window.getSize().x / 2.f, 300.f);   // Starting position
    playerCircle->setPosition(window.getSize().x / 2.f, 300.f);   // Starting position
    playerCircle->setDensity(1.f);   // Mass
    playerCircle->setVelocity(sf::Vector2f(0.f, 0.f));    // Velocity
    playerCircle->setDrag(0.4f);   // Drag
    playerCircle->setBounds(sf::IntRect(0, 0, (int)window.getSize().x, (int)window.getSize().y));

    shapes.push_back(playerCircle);
    physicsShapes.push_back(playerCircle);

    jon::ConvexShape* cs = new jon::ConvexShape(4);
    cs->setPoint(0, sf::Vector2f(0, 0));
    cs->setPoint(1, sf::Vector2f(100, 0));
    cs->setPoint(2, sf::Vector2f(150, 50));
    cs->setPoint(3, sf::Vector2f(75, 50));
    cs->setPosition(100, 100);
    cs->setFillColor(sf::Color::Red);
    cs->setDensity(1.0f);
    cs->setVelocity(sf::Vector2f(0, 0));
    cs->setDrag(0.4f);
    cs->setBounds(sf::IntRect(0, 0, (int)window.getSize().x, (int)window.getSize().y));
    //cs->setAngularVelocity(M_PI / 6.f);

    shapes.push_back(cs);
    physicsShapes.push_back(cs);

    for (unsigned int i = 0; i < 3; ++i)
    {
        jon::CircleShape* ci = new jon::CircleShape(25.f);
        ci->setFillColor(sf::Color::Green);
        //ci->setPosition(std::rand() % (int)(window.getSize().x - ci->getRadius()), std::rand() % (int)(window.getSize().y - ci->getRadius()));
        ci->setPosition(i * 25.f * 2.f, 0.f);
        //ci->setVelocity(sf::Vector2f(std::rand() % 100 + 150, std::rand() % 100 + 150));
        ci->setBounds(sf::IntRect(0, 0, (int)window.getSize().x, (int)window.getSize().y));
        ci->setDrag(0.4f);   // Drag
        ci->setDensity(1.0f);   // Mass
        ci->setVelocity(sf::Vector2f(0.f, 0.f));    // Velocity

        shapes.push_back(ci);
        physicsShapes.push_back(ci);
    }

    sf::Vector2f direction(0, 0);

    float force = 5000000.f;
    float gravity = 98.0f;

    // start the clock
    sf::Clock clock;

    while (window.isOpen())
    {
        sf::Time t = clock.restart();

        sf::Event event;
        while (window.pollEvent(event))
        {
            if (event.type == sf::Event::Closed)
                window.close();
        }
        // Clear the window
        window.clear();

        sf::Vector2f forceVector(0.f, 0.f);

        // Calculate the new acceleration
        if (sf::Keyboard::isKeyPressed(sf::Keyboard::Left))
        {
            forceVector.x -= force;
        }
        if (sf::Keyboard::isKeyPressed(sf::Keyboard::Right))
        {
            forceVector.x += force;
        }
        if (sf::Keyboard::isKeyPressed(sf::Keyboard::Up))
        {
            forceVector.y -= force;
        }
        if (sf::Keyboard::isKeyPressed(sf::Keyboard::Down))
        {
            forceVector.y += force;
        }

        sf::Vector2i mpos = sf::Mouse::getPosition(window);
        beam->setEndPoint(sf::Vector2f(mpos.x, mpos.y));

        if (sf::Mouse::isButtonPressed(sf::Mouse::Left))
        {
            for (unsigned int i = 0; i < beam->getNumRays(); ++i)
            {
                jon::Ray* ray = &(beam->getRays()[i]);
                window.draw(ray->getVertexArray(), 2, sf::Lines);
                jon::RayCollision rc = ray->checkIntersection(playerCircle);
                if (rc.collision)
                {
                    sf::Vector2f direction = playerCircle->getCentroid() - rc.enterPoint;

                    jon::Ray* b = new jon::Ray();
                    b->setStartPoint(playerCircle->getCentroid());
                    b->setEndPoint(playerCircle->getCentroid() + direction * 2.f);

                    window.draw(b->getVertexArray(), 2, sf::Lines);

                    sf::CircleShape t1c(5.f);
                    t1c.setPosition(rc.enterPoint.x - t1c.getRadius(), rc.enterPoint.y - t1c.getRadius());
                    t1c.setFillColor(sf::Color::Red);
                    window.draw(t1c);

                    sf::CircleShape t2c(5.f);
                    t2c.setPosition(rc.exitPoint.x - t2c.getRadius(), rc.exitPoint.y - t2c.getRadius());
                    t2c.setFillColor(sf::Color::Yellow);
                    window.draw(t2c);

                    delete b;
                    b = NULL;

                    // Apply a force to the object in the direction vector;
                    float rayForce = 500000.f;
                    float directionMagnitude = jon::magnitude(&direction);
                    direction.x *= rayForce / directionMagnitude;
                    direction.y *= rayForce / directionMagnitude;
                    playerCircle->addForce(direction, t);
                }
            }
        }

        sf::Vector2f playerVelocity = playerCircle->getVelocity();
        if (jon::magnitude(&playerVelocity) < 500) playerCircle->addForce(forceVector, t); // The applied force from the user
        //playerCircle->addForce(sf::Vector2f(0.f, gravity * playerCircle->getMass()), t);

        // Bounce the other shapes around
        for (unsigned int i = 0; i < physicsShapes.size(); ++i)
        {
            //physicsShapes[i]->addForce(sf::Vector2f(0.f, 0.f), t); // The applied force from the user
            if (physicsShapes[i]->isMoveable())
                physicsShapes[i]->addForce(sf::Vector2f(0.f, gravity * physicsShapes[i]->getMass()), t);
        }

        // Apply forces
        for (unsigned int i = 0; i < physicsShapes.size(); ++i)
            physicsShapes[i]->applyForces(t);

        // Detect and handle collisions
        for (unsigned int i = 0; i < physicsShapes.size(); ++i)
            jon::handleCollision(physicsShapes[i], &physicsShapes);

        // Update shapes
        for (unsigned int i = 0; i < physicsShapes.size(); ++i)
            physicsShapes[i]->updatePhysics(t);

        // Draw all the shapes
        for (unsigned int i = 0; i < shapes.size(); ++i) {
            window.draw(*(shapes[i]));
            sf::Transform t = shapes[i]->getTransform();
            sf::Vertex v(t.transformPoint(physicsShapes[i]->getCentroid()));
            //window.draw(&v, 2, sf::Lines);
        }

        // Update the FPS
        std::ostringstream result;
        result << (int)(1 / t.asSeconds());
        fps.setString(result.str());
        window.draw(fps);

        // Line
        //window.draw(ray->getVertexArray(), 2, sf::Lines);

        // Refresh the display
        window.display();
    }

    return 0;
}
